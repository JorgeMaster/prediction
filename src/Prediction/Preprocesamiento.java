package Prediction;

//Realizamos todos los imports necesarios.
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

/**
 *
 * @author Jorge Chaves Cristóbal / Isidro Jesús Delgado Alcaide 
 */
public class Preprocesamiento { //Creacion de la clase

	//Creacion de la cola donde se almacenarán las lineas que contengan más de 5 campos vacíos
    public static final Queue positionListRemove = new LinkedList<>();
    //Número de líneas de cada archivo
    public static Integer sizeRow = 0;

    //Método principal
    public static void main(String[] args) throws IOException {
    	
    	//Variable para crear un tipo distinto de csv
        int control = 1;
        //Nombre del archivo original
        String nameFile1 = "dengue_features_train";
        //Nombre del archivo a mezclar
        String nameFile2 = "dengue_labels_train";
        //Nombre del archivo de salida
        String nameOutFile = "file1";

        //Nombre del fichero test
        String nameFile3 = "dengue_features_test";
        
        //Nombre del fichero de salida de test
        String nameOutFile2 = "file2";

        //Lectura de archivos CSV
        List<String> listCSVInComplete = readFileCSV(nameFile1 + ".csv");
        List<String> listLastColum = readFileCSV(nameFile2 + ".csv");
        
        //Mezcla de los archivos
        List<String> listCSVComplete = merge(listLastColum, listCSVInComplete);
        
        //Comprobamos si las líneas tienen más de 5 campos vacíos
        validateLines(listCSVComplete);
        
        //Borramos esas lineas
        listCSVInComplete = removeIncorrectLine(listCSVComplete);
        
        //Preparamos el archivo moldeando cada linea
        String[][] matriz = prepareFile(listCSVInComplete, control, nameOutFile);
        
        //Creamos el archivo
        createFile(matriz, control, nameOutFile);
        
        //Leemos el tercer archivo
        listCSVInComplete = readFileCSV(nameFile3 + ".csv");
        
        //Comprobamos si las líneas tienen más de 5 campos vacíos
        validateLines(listCSVInComplete);
        
        //Borramos esas lineas
        listCSVInComplete = removeIncorrectLine(listCSVInComplete);
        
        //Preparamos el archivo moldeando cada linea
        String[][] matriz2 = prepareFile(listCSVInComplete, control, nameOutFile2);
        
        //Creamos el archivo
        createFile(matriz2, control, nameOutFile2);
        
    }

    public static List<String> readFileCSV(String nameFileCSV) throws IOException {
        BufferedReader br = null;
        List<String> listData = new ArrayList<>();

        try {

            br = new BufferedReader(new FileReader(nameFileCSV));
            String line = br.readLine();
            
            //Lectura de ficheros
            while (null != line) {
                String[] fields = line.split(",");

                if (nameFileCSV.equals("dengue_labels_train.csv")) {
                    listData.add(fields[fields.length - 1]); //Cogemos toda la lista menos el ultimo valor del train
                } else {
                    listData.add(line); //Cogemos todos los valores del test

                }

                line = br.readLine(); //Continuamos leyendo
            }

        } catch (Exception e) {

        } finally {
            if (null != br) {
                br.close();
            }
        }
        return listData;
    }

    private static String[][] prepareFile(List<String> listCSVInComplete, int control, String nameOutFile) throws IOException {
    	//En este método se escribirán todos aquellos campos que no tengan valor a base de las medias que contengan su registro anterior y posterior
        int i = 0;

        String[][] matrizC = new String[listCSVInComplete.size()][sizeRow];
        for (String lineTokens : listCSVInComplete) { 
        	//Asociamos a un array de cadenas todos los campos de la fila
            String[] parts = lineTokens.split(",");
            for (int j = 0; j < parts.length; j++) {
                matrizC[i][j] = parts[j];
            }

            i++;
        }

        for (int k = 1; k < matrizC.length; k++) { //Recorremos toda la matriz para rellenar esos valores
            for (int n = 1; n < matrizC[0].length; n++) {
                if (matrizC[k][n] != null) {
                    if (matrizC[k][n].equals("")) { //Si no tienen dato
                        if (k != 0 && k != matrizC.length) {
                            if (!matrizC[k + 1][n].equals("") && !matrizC[k - 1][n].equals("")) { //Si tanto el valor posterior como el anterior no están vacíos
                                //Hacemos la media
                            	double valorAnterior = Double.parseDouble(matrizC[k - 1][n]);
                                double valorPosterior = Double.parseDouble(matrizC[k + 1][n]);
                                double suma = valorAnterior + valorPosterior; 
                                matrizC[k][n] = String.valueOf(suma / 2.0);
                            } else if (matrizC[k + 1][n].equals("")) { // Si el valor posterior esta vacio, igualamos al anterior
                                matrizC[k][n] = matrizC[k - 1][n];
                            } else {
                                matrizC[k][n] = matrizC[k + 1][n]; // Caso contrario
                            }
                        }
                    }
                }
            }
        }
        return matrizC;
    }

    private static List<String> merge(List<String> listLastColum, List<String> listCSVInComplete) throws IOException {

        List<String> listCSVComplete = new ArrayList<>(); //Creamos la lista del CSV completo
        for (int i = 0; i < listLastColum.size(); i++) {
        	//Añadimos cada valor como última columna
            listCSVComplete.add(listCSVInComplete.get(i) + "," + listLastColum.get(i));
        }
        return listCSVComplete;
    }

    private static void validateLines(List<String> listCSVComplete) {
    	//Vemos en este método cuales lineas no estan completas
        sizeRow = listCSVComplete.get(0).split(",").length + 1; //Longitud de las columnas del csv
        for (int i = 0; i < listCSVComplete.size(); i++) {
            List<Integer> aux = new ArrayList<>();
            String[] lineArray = listCSVComplete.get(i).split(",");
            for (int j = 0; j < lineArray.length; j++) {
                if (lineArray[j].equals("")) {
                    aux.add(j); //Añadimos todas las filas que tienen al menos un dato vacío
                }
            }

            if (aux.size() > 5 && lineArray.length<19) {
                positionListRemove.add(i); //Añadimos unicamente aquellas lineas cuyos campos tengan menos de 5 campos vacios
            }
        }
    }

    private static List<String> removeIncorrectLine(List<String> listCSVComplete) {
        List<String> auxComplete = new ArrayList<>();//Creamos la lista final
        
        //Borramos todos aquellos registros que sean incorrectos
        if (positionListRemove.size() > 0) {
            int positionIncorrectLine = (Integer) positionListRemove.remove();

            for (int i = 0; i < listCSVComplete.size(); i++) {
                if (positionIncorrectLine != i) {
                    auxComplete.add(listCSVComplete.get(i)); //Si no se comparte la posición eliminamos ese registro porque es válido
                } else {
                    if (!positionListRemove.isEmpty()) {
                        positionIncorrectLine = (Integer) positionListRemove.remove(); //Borramos los registros 
                    }
                }
            }
        } else {
            auxComplete = listCSVComplete;
        }
        return auxComplete; //Retornamos lista final

    }

    private static void createFile(String[][] matriz, int control, String nameOutFile) {
        FileWriter fichero = null; //Creamos variable de escritura
        try {
            fichero = new FileWriter(nameOutFile + ".csv"); //Agregamos el nombre del fichero CSV

            //Establecemos el tamaño de la matriz para un archivo o para otro (train/test)
            int tamMatriz = matriz[0].length - 2;
            if(control == 1)
                tamMatriz = matriz[0].length - 1;
            
            for (int s = 0; s < matriz.length; s++) {
                if (s > 0) {
                    fichero.write("\n"); //Escribimos nueva linea
                }

                for (int h = 0; h < matriz[0].length; h++) {
                    if (h < tamMatriz) { 
                        fichero.write(matriz[s][h] + ","); //Escribimos el separador
                    } else if (h == matriz[0].length - 1 && control == 0) {
                        fichero.write(matriz[s][h-1]); //Escribimos el valor en la columna
                    } else {
                        if (control == 1) {
                            fichero.write("?"); //Escribimos interrogación únicamente en el test
                        }
                    }

                }
            }
        } catch (Exception ex) {
            System.out.println("Mensaje de la excepción: " + ex.getMessage()); //Capturamos la excepción
        }
    }


}
