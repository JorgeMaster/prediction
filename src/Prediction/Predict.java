package Prediction;


import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

import weka.classifiers.Classifier;
import weka.classifiers.trees.J48;
import weka.core.Instances;

/**
*
* @author Jorge Chaves Cristóbal / Isidro Jesús Delgado Alcaide 
*/
public class Predict {
	static List<String> prediction =new ArrayList<>();
    //public static Queue positionListRemove = new LinkedList<>();

	
    public static void main(String[] args) throws Exception {

            Classifier j48tree = new J48(); //Creamos el árbol de decisión J48
            Instances train = new Instances(new BufferedReader(new FileReader("ficheroNumerico.arff"))); //Creamos nueva instancia con el fichero train en arff
            int lastIndex = train.numAttributes() - 1;
            
            train.setClassIndex(lastIndex); //Indicamos el indice de la clase, en este caso nuestro ultimo atributo
            
            Instances test = new Instances(new BufferedReader(new FileReader("testBueno.arff")));//Creamos nueva instancia con el fichero test en arff
            test.setClassIndex(lastIndex);  //Indicamos el indice de la clase, en este caso nuestro ultimo atributo
            
            j48tree.buildClassifier(train); //Creamos el clasificador
            
            for(int i=0; i<test.numInstances(); i++) { //Para todas las instancias del test
            
                    double index = j48tree.classifyInstance(test.instance(i)); //Clasificamos esa instancia mediante el algoritmo J48
                    prediction.add(train.attribute(lastIndex).value((int)index)); //Atribuimos el valor de la predicción a cada instancia
                    System.out.println("The predicted value of instance "+Integer.toString(i)+": "+prediction.get(i)); //Imprimimos el resultado

            }
            //A continuacion es el codigo para introducirlo en el submission_format, queda comentado.
           
            /*List<String> listSubmission = Preprocesamiento.readFileCSV("submission_f.csv"); 
            
            FileWriter fichero = null; //Creamos variable de escritura
            fichero = new FileWriter("/home/jorge/workspace/Prediction/submission_format.csv"); //Agregamos el nombre del fichero CSV

            for (int i=1; i<listSubmission.size()-2; i++){
                	String [] linea = listSubmission.get(i).split(",");
                	String nombre = linea[0]+","+linea[1]+","+linea[2]+","+prediction.get(i-1)+"\n";
                	fichero.write(nombre);

                	int a = 0;
             }
            fichero.close();*/
    }
}


